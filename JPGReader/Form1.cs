﻿using JpgReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JPGReader
{
    public partial class Form1 : Form
    {
        JPEG image;
        static int RSA_P = 0;
        static int RSA_Q = 0;
        static int RSA_E = 0;
        static int d = 0;
        static int n = 0;

        static string loadImage = "";
        static string loadcipher = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            disable_all();
        }

        public string encrypt(string imageToEncrypt)
        {
            string hex = imageToEncrypt;
            char[] ar = hex.ToCharArray();
            String c = "";
          
            for (int i = 0; i < ar.Length; i++)
            {
                Application.DoEvents();
                if (c == "")
                    c = c + JPGReader.RSAalgorithm.BigMod(ar[i], RSA_E, n);
                else
                    c = c + "-" + JPGReader.RSAalgorithm.BigMod(ar[i], RSA_E, n);
            }
            return c;
        }

        public string decrypt(String imageToDecrypt)
        {
            char[] ar = imageToDecrypt.ToCharArray();
            int i = 0, j = 0;
            string c = "", dc = "";
            try
            {
                for (; i < ar.Length; i++)
                {
                    Application.DoEvents();
                    c = "";
                    for (j = i; ar[j] != '-'; j++)
                        c = c + ar[j];
                    i = j;

                    int xx = Convert.ToInt32(c);
                    dc = dc + ((char)JPGReader.RSAalgorithm.BigMod(xx, d, n)).ToString();
                }
            }
            catch (Exception ex) { }
            return dc;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog window = new OpenFileDialog();
            window.Filter = "JPG Files (*.jpg, *.jpeg)|*.jpg;*.jpeg";
            if(window.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    textBox9.Text = window.FileName;
                    button10.Enabled = true;
                    image = new JPEG(window.FileName);
                    nazwa.Text = "Nazwa:" + window.FileName;
                    wysokosc.Text = "Wysokosc: " + image.ImageHeight() + " pikseli";
                    szerokosc.Text = "Szerokosc: " + image.ImageWidth() + " pikseli";
                    precyzja.Text = "Precyzja: " + image.ImagePrecision() + " bitow";
                    wersja.Text =  image.ImageVersion();
                    rozdzielczoscPion.Text = "Rodzielczosc w pionie: " + image.imageYDensity() + " dpi";
                    rozdzielczoscPoziom.Text = "Rodzielczosc w poziomie: " + image.imageXDensity() + " dpi";
                    imageBox.Image = Image.FromFile(window.FileName);
                    imageBox.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.ToString());
                }
            }
            else
            {
                textBox9.Text = "";
                button10.Enabled = false;
            }
        }

        private void imageBox_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (button2.Text == "Ustaw wartosci")
            {
                if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "")
                {
                    MessageBox.Show("Podaj prawidłowie wartości RSA", "Błąd");
                }

                else
                {
                    if (JPGReader.Library.IsPrime(Convert.ToInt16(textBox1.Text)))
                    {
                        RSA_P = Convert.ToInt16(textBox1.Text);
                    }
                    else
                    {
                        textBox1.Text = "";
                        MessageBox.Show("Wprowadz liczbę pierwszą");
                        return;
                    }
                    if (JPGReader.Library.IsPrime(Convert.ToInt16(textBox2.Text)))
                    {
                        RSA_Q = Convert.ToInt16(textBox2.Text);
                    }
                    else
                    {
                        textBox2.Text = "";
                        MessageBox.Show("Wprowadz liczbę pierwszą");
                        return;
                    }

                    RSA_E = Convert.ToInt16(textBox3.Text);

                    textBox1.Enabled = false;
                    textBox2.Enabled = false;
                    textBox3.Enabled = false;
                    button3.Enabled = true;
                    button2.Text = "Resetuj wartości";
                }
            }
            else
            {
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox1.Enabled = true;
                textBox2.Enabled = true;
                textBox3.Enabled = true;
                button3.Enabled = false;
                button2.Text = "Ustaw wartosci";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SaveFileDialog save1 = new SaveFileDialog();
            save1.Filter = "TEXT|*.txt";
            if (save1.ShowDialog() == DialogResult.OK)
            {
                textBox4.Text = save1.FileName;
                button4.Enabled = true;
            }
            else
            {
                textBox4.Text = "";
                button4.Enabled = false;
            }
        }



        private void button4_Click(object sender, EventArgs e)
        {
            n = JPGReader.RSAalgorithm.n_value(RSA_P, RSA_Q);
            button1.Enabled = false;
            disable_all();
            String en = encrypt(loadImage);
            File.WriteAllText(textBox4.Text, en);
            MessageBox.Show("Enkrypcja wykonana!");
            button1.Enabled = true;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            button5.Enabled = false;
            disable_all();
            String de = decrypt(loadcipher);
            imageBox.Image = JPGReader.Library.ConvertByteToImage(JPGReader.Library.DecodeHex(de));
            MessageBox.Show("Deszyfrowanie ukończone!");
            imageBox.Image.Save(textBox8.Text, System.Drawing.Imaging.ImageFormat.Jpeg);
            MessageBox.Show("Obraz zapisany");
            button5.Enabled = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (button7.Text == "Ustaw wartosci")
            {
                if (textBox6.Text == "" || textBox7.Text == "")
                {
                    MessageBox.Show("Bledne wartosci do deszyfrowania RSA", "Blad");
                }
                else
                {
                    if (Convert.ToInt16(textBox6.Text) > 0)
                    {
                        d = Convert.ToInt32(textBox6.Text);
                    }
                    else
                    {
                        textBox6.Text = "";
                        MessageBox.Show("Podaj prawidlowa liczbe");
                        return;
                    }
                    if (Convert.ToInt16(textBox7.Text) > 0)
                    {
                        n = Convert.ToInt32(textBox7.Text);
                    }
                    else
                    {
                        textBox7.Text = "";
                        MessageBox.Show("Podaj prawidlowa liczbe");
                        return;
                    }

                    textBox6.Enabled = false;
                    textBox7.Enabled = false;
                    button7.Text = "Resetuj wartosci";
                    button9.Enabled = true;

                }
            }
            else
            {
                textBox6.Text = "";
                textBox7.Text = "";
                textBox6.Enabled = true;
                textBox7.Enabled = true;
                button7.Text = "Ustaw wartosci";
                button9.Enabled = false;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            loadImage = BitConverter.ToString(JPGReader.Library.ConvertImageToByte(imageBox.Image));
            MessageBox.Show("Obrazek załadowany pomyślnie!");
            groupBox1.Enabled = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            OpenFileDialog open1 = new OpenFileDialog();
            open1.Filter = "TEXT|*.txt";
            if (open1.ShowDialog() == DialogResult.OK)
            {
                textBox5.Text = open1.FileName;
                button6.Enabled = true;
            }
            else
            {
                textBox5.Text = "";
                button6.Enabled = false;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            loadcipher = File.ReadAllText(textBox5.Text);
            MessageBox.Show("Plik załadowany pomyślnie");
           groupBox2.Enabled = true;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            SaveFileDialog save1 = new SaveFileDialog();
            save1.Filter = "JPG|*.JPG";
            if (save1.ShowDialog() == DialogResult.OK)
            {
                textBox8.Text = save1.FileName;
                button8.Enabled = true;
            }
            else
            {
                textBox8.Text = "";
                button8.Enabled = false;
            }
        }

        private void disable_all()
        {
            button10.Enabled = false;
            groupBox1.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;

            button6.Enabled = false;
            groupBox2.Enabled = false;
            button9.Enabled = false;
            button8.Enabled = false;
        }

        private void enable_all()
        {
            button1.Enabled = true;
            button10.Enabled = true;
            groupBox1.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            loadImage = BitConverter.ToString(JPGReader.Library.ConvertImageToByte(imageBox.Image));
            MessageBox.Show("Obrazek załadowany poprawnie");
            groupBox1.Enabled = true;
        }
    }
}
