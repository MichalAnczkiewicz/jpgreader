﻿using JPGReader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JpgReader
{
    class JPEG : Form1
    {
        List<List<byte>> headers = new List<List<byte>>(); //USUALLY is only one header
        List<List<byte>> quantizationTables = new List<List<byte>>(); //USUALLY two
        List<List<byte>> huffmanTables = new List<List<byte>>(); //USUALLY three or four
        List<List<byte>> frames = new List<List<byte>>();
        List<List<byte>> scans = new List<List<byte>>();

        public Header header;

        public Frame frame;

        JPEG() { }
        public JPEG(string path)
        {
            LoadData(path);
        }

        void LoadData(string path)
        {
            byte[] tablica = File.ReadAllBytes(path);
            if (!(tablica[0] == 0x00FF && tablica[1] == 0x00D8))
                throw new System.Exception("Providen file is not a JPEG!!!");
            DivideData(tablica);
            if (headers.Count > 1)
                throw new System.Exception("More than one header in JPEG - can't handle with it!!!");
            if (headers.Count == 0)
                throw new System.Exception("No header found, something wrong with jpeg!!!");
            header = new Header(headers[0]);
            if (frames.Count > 1)
                throw new System.Exception("More than one frame in JPEG - can't handle with it!!!");
            if (frames.Count == 0)
                throw new System.Exception("No frame found, something wrong with jpeg!!!");
            frame = new Frame(frames[0]);
        }

        void DivideData(byte[] tablica)
        {
            for (int i = 2; i < tablica.Length; i++)
            {
                if (tablica[i] == 0x00FE && tablica[i - 1] == 0x00FF) //comments
                {
                    i++;
                    while (tablica[i] != 0x00ff)
                        i++;
                }
                else if ((tablica[i] == 0x00DB || tablica[i] == 0x00C4 || tablica[i] == 0x00C0 || tablica[i] == 0x00DA || tablica[i] == 0x00E0)
                    && tablica[i - 1] == 0x00FF)
                {
                    var type = tablica[i];
                    i++;
                    List<byte> table = new List<byte>();
                    while (tablica[i] != 0x00ff)
                    {
                        table.Add(tablica[i]);
                        i++;
                    }
                    if (type == 0x00DB)
                        quantizationTables.Add(table);
                    else if (type == 0x00C4)
                        huffmanTables.Add(table);
                    else if (type == 0x00C0)
                        frames.Add(table);
                    else if (type == 0x00DA)
                        scans.Add(table);
                    else if (type == 0x00E0)
                        headers.Add(table);
                }
            }
        }

        public int ImageHeight()
        {
            int imageHeight = ByteSum(frame.height[0], frame.height[1]);
            return imageHeight;
        }

        public int ImagePrecision()
        {
            int imgPrec = frame.precision;
            return imgPrec;
        }

        public int ImageWidth()
        {
            int imageWidth = ByteSum(frame.width[0], frame.width[1]);
            return imageWidth;
        }

        public int imageXDensity()
        {   

            int imageXDensity = ByteSum(header.xDensity[0], header.xDensity[1]);
            return imageXDensity;
        }
        public int imageYDensity()
        {
            int imageYDensity = ByteSum(header.yDensity[0], header.yDensity[1]);
            return imageYDensity;
        }

        public String ImageVersion()
        {
            string imageVersion = System.String.Format("Wersja jpeg: {0:d}.{1:d} ", header.version[0], header.version[1]);
            return imageVersion;
        }



        public static int ByteSum(byte b1, byte b2)
        {
            return (b1 << 8) + (int)b2;
        }

    }

    class Header
    {
        public byte[] length = new byte[2]; //dlugosc naglowka (ilosc bajtow)
        public byte[] identifier = new byte[5]; //is used to identify the code stream as conforming to the JFIF specification
        public byte[] version = new byte[2]; //numer wersji np. 01h 02h to wersja 1.2
        public byte units; //jednostka uzywana do okreslenia rozdzielczosci: 0-piksele, 1-dots per inch, 2-dots per cm
        public byte[] xDensity = new byte[2]; //pozioma rozdzielczosc
        public byte[] yDensity = new byte[2]; //pionowa rozdzielczosc
        public byte xThumbnail;
        public byte yThumbnail;

        Header() { }
        public Header(List<byte> data)
        {
            length[0] = data[0];
            length[1] = data[1];
            for (int i = 2; i < 7; i++)
                identifier[i - 2] = data[i];
            version[0] = data[7];
            version[1] = data[8];
            units = data[9];
            xDensity[0] = data[10];
            xDensity[1] = data[11];
            yDensity[0] = data[12];
            yDensity[1] = data[13];
            xThumbnail = data[14];
            yThumbnail = data[15];
        }

        public void PrintData()
        {
            Console.WriteLine("Wersja jpeg: {0:d}.{1:d}", version[0], version[1]);

            if (units == 0x0001)
            {
                Console.WriteLine("Rozdzielczosc w poziomie: {0:d} dpi",JPEG.ByteSum(xDensity[0],xDensity[1]));
                Console.WriteLine("Rozdzielczosc w pionie: {0:d} dpi", JPEG.ByteSum(yDensity[0], yDensity[1]));
            }
            else if(units == 0x0002)
            {
                Console.WriteLine("Rozdzielczosc w poziomie: {0:d} dots per cm", JPEG.ByteSum(xDensity[0], xDensity[1]));
                Console.WriteLine("Rozdzielczosc w pionie: {0:d} dots per cm", JPEG.ByteSum(yDensity[0], yDensity[1]));
            }
        }
    }

    class Frame 
       
    {
        public byte[] length = new byte[2]; //dlugosc Frame (ilosc bajtow)
        public byte precision; //precyzja probki (ilosc bitow na probke)
        public byte[] height = new byte[2]; //wysokosc obrazu (w pikselach)
        public byte[] width = new byte[2]; //szerokosc obrazu (w pikselach)
        public byte componentAmount; //1-skala szarosci, 3-kolor YCbCr (Y-skladowa luminacji, Cb-skladowa roznicowa chrominacji Y-B)
        public List<Component> components = new List<Component>();

        Frame() { }
        public Frame(List<byte> data)
        {
            length[0] = data[0];
            length[1] = data[1];
            precision = data[2];
            height[0] = data[3];
            height[1] = data[4];
            width[0] = data[5];
            width[1] = data[6];
            componentAmount = data[7];
            if(componentAmount == 0x0001)
                components.Add(new Component(data[8], data[9], data[10]));
            else
                for(int i=8; i<17; i += 3)
                    components.Add( new Component(data[i], data[i + 1], data[i + 2]) );
        }

       

        public void PrintData()
        {
            if (componentAmount == 0x0001)
            {
                Console.WriteLine("Obraz w skali szarosci, parametry komponentu:");
                Console.WriteLine("\tCzestotliwosc probkowania: {0:d}", components[0].samplingRate);
                if (components[0].qTable == 0x0000)
                    Console.WriteLine("\tTablica kwantyzacji: pierwsza");
                else
                    Console.WriteLine("\tTablica kwantyzacji: druga");
            }
            else
            {
                Console.WriteLine("Obraz kolorowy, parametry komponentow:");
                for (int i = 0; i < 3; i++)
                {
                    if (i == 0)
                        Console.WriteLine("\tKomponent Y:");
                    else if (i == 1)
                        Console.WriteLine("\tKomponent Cb:");
                    else
                        Console.WriteLine("\tKomponent Cr:");
                    Console.WriteLine("\t\tCzestotliwosc probkowania: {0:d}", components[i].samplingRate);
                    if (components[i].qTable == 0x0000)
                        Console.WriteLine("\t\tTablica kwantyzacji: pierwsza");
                    else
                        Console.WriteLine("\t\tTablica kwantyzacji: druga");
                }
            }
        }
    }

    class Component
    {
        public byte id; //id komponentu
        public byte samplingRate; //czestotliwosc probkowania po szerokosci i dlugosci
        public byte qTable; //Numer tablicy kwantyzacji 0-pierwsza, 1-druga

        Component() { }
        public Component(byte id, byte sr, byte qt)
        {
            this.id = id;
            samplingRate = sr;
            qTable = qt;
        }
    }

}


